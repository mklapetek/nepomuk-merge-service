#ifndef _PIMO_NOTE_H_
#define _PIMO_NOTE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/document.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A note. The textual contents of the note should be expressed 
 * in the nao:description value of the note. 
 */
class Note : public PIMO::Document
{
public:
    Note(Nepomuk::SimpleResource* res)
      : PIMO::Document(res), m_res(res)
    {}

    virtual ~Note() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Note", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
