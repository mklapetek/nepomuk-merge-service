#ifndef _PIMO_PERSON_H_
#define _PIMO_PERSON_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/agent.h"
#include "pimo/locatable.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Represents a person. Either living, dead, real or imaginary. 
 * (Definition from foaf:Person) 
 */
class Person : public PIMO::Agent, public PIMO::Locatable
{
public:
    Person(Nepomuk::SimpleResource* res)
      : PIMO::Agent(res), PIMO::Locatable(res), m_res(res)
    {}

    virtual ~Person() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends. 
     * A person attends a social event. 
     */
    QList<QUrl> attendses() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends. 
     * A person attends a social event. 
     */
    void setAttendses(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends. 
     * A person attends a social event. 
     */
    void addAttends(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attends", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId. 
     * Jabber-ID of the user. Used to communicate amongst peers in 
     * the social scenario of the semantic desktop. Use the xmpp node 
     * identifier as specified by RFC3920, see http://www.xmpp.org/specs/rfc3920.html#addressing-node. 
     * The format is the same as e-mail addresses: username@hostname. 
     */
    QString jabberId() const {
        QString value;
        if(m_res->contains(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId", QUrl::StrictMode)))
            value = m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId", QUrl::StrictMode)).first().value<QString>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId. 
     * Jabber-ID of the user. Used to communicate amongst peers in 
     * the social scenario of the semantic desktop. Use the xmpp node 
     * identifier as specified by RFC3920, see http://www.xmpp.org/specs/rfc3920.html#addressing-node. 
     * The format is the same as e-mail addresses: username@hostname. 
     */
    void setJabberId(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        values << value;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId. 
     * Jabber-ID of the user. Used to communicate amongst peers in 
     * the social scenario of the semantic desktop. Use the xmpp node 
     * identifier as specified by RFC3920, see http://www.xmpp.org/specs/rfc3920.html#addressing-node. 
     * The format is the same as e-mail addresses: username@hostname. 
     */
    void addJabberId(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#jabberId", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Person", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
