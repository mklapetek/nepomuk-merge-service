#ifndef _PIMO_DOCUMENT_H_
#define _PIMO_DOCUMENT_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/logicalmediatype.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A generic document. This is a placeholder class for document-management 
 * domain ontologies to subclass. Create more and specified subclasses 
 * of pimo:Document for the document types in your domain. Documents 
 * are typically instances of both NFO:Document (modeling the 
 * information element used to store the document) and a LogicalMediaType 
 * subclass. Two examples are given for what to model here: a contract 
 * for a business domain, a BlogPost for an informal domain. 
 */
class Document : public PIMO::LogicalMediaType
{
public:
    Document(Nepomuk::SimpleResource* res)
      : PIMO::LogicalMediaType(res), m_res(res)
    {}

    virtual ~Document() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Document", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
