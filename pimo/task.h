#ifndef _PIMO_TASK_H_
#define _PIMO_TASK_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/processconcept.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A (usually assigned) piece of work (often to be finished within 
 * a certain time). (Definition from Merriam-Webster) 
 */
class Task : public PIMO::ProcessConcept
{
public:
    Task(Nepomuk::SimpleResource* res)
      : PIMO::ProcessConcept(res), m_res(res)
    {}

    virtual ~Task() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime. 
     * when is this task due? Represented in ISO 8601, example: 2003-11-22T17:00:00 
     */
    QDateTime taskDueTime() const {
        QDateTime value;
        if(m_res->contains(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime", QUrl::StrictMode)))
            value = m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime", QUrl::StrictMode)).first().value<QDateTime>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime. 
     * when is this task due? Represented in ISO 8601, example: 2003-11-22T17:00:00 
     */
    void setTaskDueTime(const QDateTime& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        values << value;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime. 
     * when is this task due? Represented in ISO 8601, example: 2003-11-22T17:00:00 
     */
    void addTaskDueTime(const QDateTime& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#taskDueTime", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Task", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
