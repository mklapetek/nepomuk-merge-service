#ifndef _PIMO_MEETING_H_
#define _PIMO_MEETING_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/socialevent.h"
namespace Nepomuk {
namespace PIMO {
/**
 * The social act of assembling for some common purpose; "his meeting 
 * with the salesman was the high point of his day". (Definition 
 * from SUMO) 
 */
class Meeting : public PIMO::SocialEvent
{
public:
    Meeting(Nepomuk::SimpleResource* res)
      : PIMO::SocialEvent(res), m_res(res)
    {}

    virtual ~Meeting() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Meeting", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
