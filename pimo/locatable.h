#ifndef _PIMO_LOCATABLE_H_
#define _PIMO_LOCATABLE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Things that can be at a location. Abstract class, use it as a superclass 
 * of things that can be placed in physical space. 
 */
class Locatable : public PIMO::Thing
{
public:
    Locatable(Nepomuk::SimpleResource* res)
      : PIMO::Thing(res), m_res(res)
    {}

    virtual ~Locatable() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation. 
     * The subject thing is currently located at the object location. 
     */
    QUrl hasLocation() const {
        QUrl value;
        if(m_res->contains(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation", QUrl::StrictMode)))
            value = m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation", QUrl::StrictMode)).first().value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation. 
     * The subject thing is currently located at the object location. 
     */
    void setHasLocation(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        values << value;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation. 
     * The subject thing is currently located at the object location. 
     */
    void addHasLocation(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasLocation", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Locatable", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
