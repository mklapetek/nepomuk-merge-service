#ifndef _PIMO_AGENT_H_
#define _PIMO_AGENT_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * An agent (eg. person, group, software or physical artifact). 
 * The Agent class is the class of agents; things that do stuff. 
 * A well known sub-class is Person, representing people. Other 
 * kinds of agents include Organization and Group. (inspired 
 * by FOAF). Agent is not a subclass of NAO:Party. 
 */
class Agent : public PIMO::Thing
{
public:
    Agent(Nepomuk::SimpleResource* res)
      : PIMO::Thing(res), m_res(res)
    {}

    virtual ~Agent() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf. 
     * The subject person or organozation (Agent) is member of the 
     * object organization. 
     */
    QList<QUrl> isOrganizationMemberOfs() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf. 
     * The subject person or organozation (Agent) is member of the 
     * object organization. 
     */
    void setIsOrganizationMemberOfs(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf. 
     * The subject person or organozation (Agent) is member of the 
     * object organization. 
     */
    void addIsOrganizationMemberOf(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isOrganizationMemberOf", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo. 
     * The creator of the Personal Information Model. The human being 
     * whose mental models are represented in the PIMO. 
     */
    QList<QUrl> createdPimos() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo. 
     * The creator of the Personal Information Model. The human being 
     * whose mental models are represented in the PIMO. 
     */
    void setCreatedPimos(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo. 
     * The creator of the Personal Information Model. The human being 
     * whose mental models are represented in the PIMO. 
     */
    void addCreatedPimo(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#createdPimo", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Agent", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
