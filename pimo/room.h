#ifndef _PIMO_ROOM_H_
#define _PIMO_ROOM_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/location.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A properPart of a Building which is separated from the exterior 
 * of the Building and/or other Rooms of the Building by walls. 
 * Some Rooms may have a specific purpose, e.g. sleeping, bathing, 
 * cooking, entertainment, etc. (Definition from SUMO). 
 */
class Room : public PIMO::Location
{
public:
    Room(Nepomuk::SimpleResource* res)
      : PIMO::Location(res), m_res(res)
    {}

    virtual ~Room() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Room", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
