#ifndef _PIMO_PROJECT_H_
#define _PIMO_PROJECT_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/processconcept.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Any piece of work that is undertaken or attempted (Wordnet). 
 * An enterprise carefully planned to achieve a particular aim 
 * (Oxford Dictionary). 
 */
class Project : public PIMO::ProcessConcept
{
public:
    Project(Nepomuk::SimpleResource* res)
      : PIMO::ProcessConcept(res), m_res(res)
    {}

    virtual ~Project() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Project", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
