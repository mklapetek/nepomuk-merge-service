#ifndef _PIMO_CLASSORTHING_H_
#define _PIMO_CLASSORTHING_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/classorthingorpropertyorassociation.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Superclass of class and thing. To add properties to both class 
 * and thing. 
 */
class ClassOrThing : public PIMO::ClassOrThingOrPropertyOrAssociation
{
public:
    ClassOrThing(Nepomuk::SimpleResource* res)
      : PIMO::ClassOrThingOrPropertyOrAssociation(res), m_res(res)
    {}

    virtual ~ClassOrThing() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder. 
     * Folders can be used to store information elements related to 
     * a Thing or Class. This property can be used to connect a Class 
     * or Thing to existing Folders. Implementations can suggest 
     * annotations for documents stored inside these folders or suggest 
     * the folder for new documents related to the Thing or Class. 
     */
    QList<QUrl> hasFolders() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder. 
     * Folders can be used to store information elements related to 
     * a Thing or Class. This property can be used to connect a Class 
     * or Thing to existing Folders. Implementations can suggest 
     * annotations for documents stored inside these folders or suggest 
     * the folder for new documents related to the Thing or Class. 
     */
    void setHasFolders(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder. 
     * Folders can be used to store information elements related to 
     * a Thing or Class. This property can be used to connect a Class 
     * or Thing to existing Folders. Implementations can suggest 
     * annotations for documents stored inside these folders or suggest 
     * the folder for new documents related to the Thing or Class. 
     */
    void addHasFolder(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasFolder", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText. 
     * A wiki-like free-text description of a Thing or a Class. The 
     * text can be formatted using a limited set of HTML elements and 
     * can contain links to other Things. The format is described in 
     * detail in the WIF specification (http://semanticweb.org/wiki/Wiki_Interchange_Format). 
     */
    QString wikiText() const {
        QString value;
        if(m_res->contains(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText", QUrl::StrictMode)))
            value = m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText", QUrl::StrictMode)).first().value<QString>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText. 
     * A wiki-like free-text description of a Thing or a Class. The 
     * text can be formatted using a limited set of HTML elements and 
     * can contain links to other Things. The format is described in 
     * detail in the WIF specification (http://semanticweb.org/wiki/Wiki_Interchange_Format). 
     */
    void setWikiText(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        values << value;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText. 
     * A wiki-like free-text description of a Thing or a Class. The 
     * text can be formatted using a limited set of HTML elements and 
     * can contain links to other Things. The format is described in 
     * detail in the WIF specification (http://semanticweb.org/wiki/Wiki_Interchange_Format). 
     */
    void addWikiText(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#wikiText", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#ClassOrThing", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
