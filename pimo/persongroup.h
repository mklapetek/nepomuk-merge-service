#ifndef _PIMO_PERSONGROUP_H_
#define _PIMO_PERSONGROUP_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/agent.h"
#include "pimo/collection.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A group of Persons. They are connected to each other by sharing 
 * a common attribute, for example they all belong to the same organization 
 * or have a common interest. Refer to pimo:Collection for more 
 * information about defining collections. 
 */
class PersonGroup : public PIMO::Agent, public PIMO::Collection
{
public:
    PersonGroup(Nepomuk::SimpleResource* res)
      : PIMO::Agent(res), PIMO::Collection(res), m_res(res)
    {}

    virtual ~PersonGroup() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#PersonGroup", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
