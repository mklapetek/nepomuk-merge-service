#ifndef _PIMO_ORGANIZATION_H_
#define _PIMO_ORGANIZATION_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/locatable.h"
#include "pimo/agent.h"
namespace Nepomuk {
namespace PIMO {
/**
 * An administrative and functional structure (as a business 
 * or a political party). (Definition from Merriam-Webster) 
 */
class Organization : public PIMO::Locatable, public PIMO::Agent
{
public:
    Organization(Nepomuk::SimpleResource* res)
      : PIMO::Locatable(res), PIMO::Agent(res), m_res(res)
    {}

    virtual ~Organization() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember. 
     * The subject organization has the object person or organization 
     * (Agent) as a member. 
     */
    QList<QUrl> hasOrganizationMembers() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember. 
     * The subject organization has the object person or organization 
     * (Agent) as a member. 
     */
    void setHasOrganizationMembers(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember. 
     * The subject organization has the object person or organization 
     * (Agent) as a member. 
     */
    void addHasOrganizationMember(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#hasOrganizationMember", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Organization", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
