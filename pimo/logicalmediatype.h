#ifndef _PIMO_LOGICALMEDIATYPE_H_
#define _PIMO_LOGICALMEDIATYPE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Logical media types represent the content aspect of information 
 * elements e.g. a flyer, a contract, a promotional video, a todo 
 * list. The user can create new logical media types dependend 
 * on their domain: a salesman will need MarketingFlyer, Offer, 
 * Invoice while a student might create Report, Thesis and Homework. 
 * This is independent from the information element and data object 
 * (NIE/NFO) in which the media type will be stored. The same contract 
 * can be stored in a PDF file, a text file, or an HTML website. The 
 * groundingOccurrence of a LogicalMediaType is the Document 
 * that stores the content. 
 */
class LogicalMediaType : public PIMO::Thing
{
public:
    LogicalMediaType(Nepomuk::SimpleResource* res)
      : PIMO::Thing(res), m_res(res)
    {}

    virtual ~LogicalMediaType() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#LogicalMediaType", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
