#ifndef _PIMO_CONTRACT_H_
#define _PIMO_CONTRACT_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/document.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A binding agreement between two or more persons that is enforceable 
 * by law. (Definition from SUMO). This is an example class for 
 * a document type, there are more detailled ontologies to model 
 * Contracts. 
 */
class Contract : public PIMO::Document
{
public:
    Contract(Nepomuk::SimpleResource* res)
      : PIMO::Document(res), m_res(res)
    {}

    virtual ~Contract() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Contract", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
