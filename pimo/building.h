#ifndef _PIMO_BUILDING_H_
#define _PIMO_BUILDING_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/location.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A structure that has a roof and walls and stands more or less permanently 
 * in one place; "there was a three-story building on the corner"; 
 * "it was an imposing edifice". (Definition from SUMO). 
 */
class Building : public PIMO::Location
{
public:
    Building(Nepomuk::SimpleResource* res)
      : PIMO::Location(res), m_res(res)
    {}

    virtual ~Building() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Building", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
