#ifndef _PIMO_TAG_H_
#define _PIMO_TAG_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Tags in the context of PIMO. A marker class for Things that are 
 * used to categorize documents (or other things). Tags must be 
 * a kind of Thing and must have a unique label. Documents should 
 * not be Tags by default. 
 */
class Tag : public PIMO::Thing
{
public:
    Tag(Nepomuk::SimpleResource* res)
      : PIMO::Thing(res), m_res(res)
    {}

    virtual ~Tag() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor. 
     * This thing is described further in the object thing. Similar 
     * semantics as skos:isSubjectOf. 
     */
    QList<QUrl> isTagFors() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor. 
     * This thing is described further in the object thing. Similar 
     * semantics as skos:isSubjectOf. 
     */
    void setIsTagFors(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor. 
     * This thing is described further in the object thing. Similar 
     * semantics as skos:isSubjectOf. 
     */
    void addIsTagFor(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isTagFor", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel. 
     * The unique label of the tag. The label must be unique within the 
     * scope of one PersonalInformationModel. It is required and 
     * a subproperty of nao:prefLabel. It clarifies the use of nao:personalIdentifier 
     * by restricting the scope to tags. Semantically equivalent 
     * to skos:prefLabel, where uniqueness of labels is also recommended. 
     */
    QString tagLabel() const {
        QString value;
        if(m_res->contains(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel", QUrl::StrictMode)))
            value = m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel", QUrl::StrictMode)).first().value<QString>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel. 
     * The unique label of the tag. The label must be unique within the 
     * scope of one PersonalInformationModel. It is required and 
     * a subproperty of nao:prefLabel. It clarifies the use of nao:personalIdentifier 
     * by restricting the scope to tags. Semantically equivalent 
     * to skos:prefLabel, where uniqueness of labels is also recommended. 
     */
    void setTagLabel(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        values << value;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel. 
     * The unique label of the tag. The label must be unique within the 
     * scope of one PersonalInformationModel. It is required and 
     * a subproperty of nao:prefLabel. It clarifies the use of nao:personalIdentifier 
     * by restricting the scope to tags. Semantically equivalent 
     * to skos:prefLabel, where uniqueness of labels is also recommended. 
     */
    void addTagLabel(const QString& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#tagLabel", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Tag", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
