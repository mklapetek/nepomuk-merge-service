#ifndef _PIMO_STATE_H_
#define _PIMO_STATE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/location.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Administrative subdivisions of a Nation that are broader than 
 * any other political subdivisions that may exist. This Class 
 * includes the states of the United States, as well as the provinces 
 * of Canada and European countries. (Definition from SUMO). 
 */
class State : public PIMO::Location
{
public:
    State(Nepomuk::SimpleResource* res)
      : PIMO::Location(res), m_res(res)
    {}

    virtual ~State() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#State", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
