#ifndef _PIMO_TOPIC_H_
#define _PIMO_TOPIC_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/tag.h"
#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A topic is the subject of a discussion or document. Topics are 
 * distinguished from Things in their taxonomic nature, examples 
 * are scientific areas such as "Information Science", "Biology", 
 * or categories used in content syndication such as "Sports", 
 * "Politics". They are specific to the user's domain. 
 */
class Topic : public PIMO::Tag, public PIMO::Thing
{
public:
    Topic(Nepomuk::SimpleResource* res)
      : PIMO::Tag(res), PIMO::Thing(res), m_res(res)
    {}

    virtual ~Topic() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic. 
     * The object topic is more general in meaning than the subject 
     * topic. Transitive. Similar to skos:broader. 
     */
    QList<QUrl> superTopics() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic. 
     * The object topic is more general in meaning than the subject 
     * topic. Transitive. Similar to skos:broader. 
     */
    void setSuperTopics(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic. 
     * The object topic is more general in meaning than the subject 
     * topic. Transitive. Similar to skos:broader. 
     */
    void addSuperTopic(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#superTopic", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic. 
     * The object topic is more specific in meaning than the subject 
     * topic. Transitive. Similar in meaning to skos:narrower 
     */
    QList<QUrl> subTopics() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic. 
     * The object topic is more specific in meaning than the subject 
     * topic. Transitive. Similar in meaning to skos:narrower 
     */
    void setSubTopics(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic. 
     * The object topic is more specific in meaning than the subject 
     * topic. Transitive. Similar in meaning to skos:narrower 
     */
    void addSubTopic(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#subTopic", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Topic", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
