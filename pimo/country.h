#ifndef _PIMO_COUNTRY_H_
#define _PIMO_COUNTRY_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/location.h"
namespace Nepomuk {
namespace PIMO {
/**
 * The territory occupied by a nation; "he returned to the land 
 * of his birth"; "he visited several European countries". (Definition 
 * from SUMO) 
 */
class Country : public PIMO::Location
{
public:
    Country(Nepomuk::SimpleResource* res)
      : PIMO::Location(res), m_res(res)
    {}

    virtual ~Country() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Country", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
