#ifndef _PIMO_LOCATION_H_
#define _PIMO_LOCATION_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/thing.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A physical location. Subclasses are modeled for the most common 
 * locations humans work in: Building, City, Country, Room, State. 
 * This selection is intended to be applicable cross-cultural 
 * and cross-domain. City is a prototype that can be further refined 
 * for villages, etc. Subclass of a WGS84:SpatialThing, can have 
 * geo-coordinates. 
 */
class Location : public PIMO::Thing
{
public:
    Location(Nepomuk::SimpleResource* res)
      : PIMO::Thing(res), m_res(res)
    {}

    virtual ~Location() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation. 
     * The subject location contains the object location. For example, 
     * a building contains a room or a country contains a city. 
     */
    QList<QUrl> containsLocations() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation. 
     * The subject location contains the object location. For example, 
     * a building contains a room or a country contains a city. 
     */
    void setContainsLocations(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation. 
     * The subject location contains the object location. For example, 
     * a building contains a room or a country contains a city. 
     */
    void addContainsLocation(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#containsLocation", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf. 
     * The subject location is the current location of the object. 
     */
    QList<QUrl> isLocationOfs() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf. 
     * The subject location is the current location of the object. 
     */
    void setIsLocationOfs(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf. 
     * The subject location is the current location of the object. 
     */
    void addIsLocationOf(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#isLocationOf", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin. 
     * The subject location is contained within the object location. 
     * For example, a room is located within a building or a city is located 
     * within a country. 
     */
    QList<QUrl> locatedWithins() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin. 
     * The subject location is contained within the object location. 
     * For example, a room is located within a building or a city is located 
     * within a country. 
     */
    void setLocatedWithins(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin. 
     * The subject location is contained within the object location. 
     * For example, a room is located within a building or a city is located 
     * within a country. 
     */
    void addLocatedWithin(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#locatedWithin", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Location", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
