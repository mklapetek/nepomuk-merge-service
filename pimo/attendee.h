#ifndef _PIMO_ATTENDEE_H_
#define _PIMO_ATTENDEE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/personrole.h"
namespace Nepomuk {
namespace PIMO {
/**
 * The role of someone attending a social event. 
 */
class Attendee : public PIMO::PersonRole
{
public:
    Attendee(Nepomuk::SimpleResource* res)
      : PIMO::PersonRole(res), m_res(res)
    {}

    virtual ~Attendee() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting. 
     * the attended meeting 
     */
    QList<QUrl> attendingMeetings() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting. 
     * the attended meeting 
     */
    void setAttendingMeetings(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting. 
     * the attended meeting 
     */
    void addAttendingMeeting(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#attendingMeeting", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Attendee", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
