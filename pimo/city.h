#ifndef _PIMO_CITY_H_
#define _PIMO_CITY_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/location.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A large and densely populated urban area; may include several 
 * independent administrative districts; "Ancient Troy was 
 * a great city". (Definition from SUMO) 
 */
class City : public PIMO::Location
{
public:
    City(Nepomuk::SimpleResource* res)
      : PIMO::Location(res), m_res(res)
    {}

    virtual ~City() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#City", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
