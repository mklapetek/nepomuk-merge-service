#ifndef _PIMO_EVENT_H_
#define _PIMO_EVENT_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/processconcept.h"
namespace Nepomuk {
namespace PIMO {
/**
 * Something that happens An Event is conceived as compact in time. 
 * (Definition from Merriam-Webster) 
 */
class Event : public PIMO::ProcessConcept
{
public:
    Event(Nepomuk::SimpleResource* res)
      : PIMO::ProcessConcept(res), m_res(res)
    {}

    virtual ~Event() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#Event", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
