#ifndef _PIMO_ORGANIZATIONMEMBER_H_
#define _PIMO_ORGANIZATIONMEMBER_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/personrole.h"
namespace Nepomuk {
namespace PIMO {
/**
 * The role of one or multiple persons being a member in one or multiple 
 * organizations. Use pimo:organization and pimo:roleHolder 
 * to link to the organizations and persons. 
 */
class OrganizationMember : public PIMO::PersonRole
{
public:
    OrganizationMember(Nepomuk::SimpleResource* res)
      : PIMO::PersonRole(res), m_res(res)
    {}

    virtual ~OrganizationMember() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization. 
     * relation to the organization in an OrganizationMember association. 
     */
    QList<QUrl> organizations() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization. 
     * relation to the organization in an OrganizationMember association. 
     */
    void setOrganizations(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization. 
     * relation to the organization in an OrganizationMember association. 
     */
    void addOrganization(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#organization", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#OrganizationMember", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
