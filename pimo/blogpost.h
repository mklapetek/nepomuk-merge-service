#ifndef _PIMO_BLOGPOST_H_
#define _PIMO_BLOGPOST_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/document.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A blog note. You just want to write something down right now and 
 * need a place to do that. Add a blog-note! This is an example class 
 * for a document type, there are more detailled ontologies to 
 * model Blog-Posts (like SIOC). 
 */
class BlogPost : public PIMO::Document
{
public:
    BlogPost(Nepomuk::SimpleResource* res)
      : PIMO::Document(res), m_res(res)
    {}

    virtual ~BlogPost() {}

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#BlogPost", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
