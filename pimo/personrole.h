#ifndef _PIMO_PERSONROLE_H_
#define _PIMO_PERSONROLE_H_

#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QDate>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <Soprano/Vocabulary/RDF>

#include <nepomuk/simpleresource.h>

#include "pimo/association.h"
namespace Nepomuk {
namespace PIMO {
/**
 * A person takes a certain role in a given context. The role can 
 * be that of "a mentor or another person" or "giving a talk at a meeting", 
 * etc. 
 */
class PersonRole : public PIMO::Association
{
public:
    PersonRole(Nepomuk::SimpleResource* res)
      : PIMO::Association(res), m_res(res)
    {}

    virtual ~PersonRole() {}

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder. 
     * the person taking the role 
     */
    QList<QUrl> roleHolders() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder. 
     * the person taking the role 
     */
    void setRoleHolders(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder. 
     * the person taking the role 
     */
    void addRoleHolder(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleHolder", QUrl::StrictMode), value);
    }

    /**
     * Get property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext. 
     * The context where the role-holder impersonates this role. 
     * For example, the company where a person is employed. 
     */
    QList<QUrl> roleContexts() const {
        QList<QUrl> value;
        foreach(const QVariant& v, m_res->property(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext", QUrl::StrictMode)))
            value << v.value<QUrl>();
        return value;
    }

    /**
     * Set property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext. 
     * The context where the role-holder impersonates this role. 
     * For example, the company where a person is employed. 
     */
    void setRoleContexts(const QList<QUrl>& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        QVariantList values;
        foreach(const QUrl& v, value)
            values << v;
        m_res->setProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext", QUrl::StrictMode), values);
    }

    /**
     * Add value to property http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext. 
     * The context where the role-holder impersonates this role. 
     * For example, the company where a person is employed. 
     */
    void addRoleContext(const QUrl& value) {
        m_res->addProperty(Soprano::Vocabulary::RDF::type(), resourceType());
        m_res->addProperty(QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#roleContext", QUrl::StrictMode), value);
    }

protected:
    virtual QUrl resourceType() const { return QUrl::fromEncoded("http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#PersonRole", QUrl::StrictMode); }

private:
    Nepomuk::SimpleResource* m_res;
};
}
}

#endif
