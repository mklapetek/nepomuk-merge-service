/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Vishesh Handa <handa.vish@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QPair>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/PIMO>
#include <Nepomuk/Variant>
#include <Nepomuk/Thing>

#include <nepomuk/datamanagement.h>
#include <nepomuk/resourcewatcher.h>
#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>

#include <Soprano/QueryResultIterator>
#include <Soprano/Node>
#include <Soprano/Model>

#include "pimo/tag.h"
#include "pimo/person.h"

#include "mergeservice.h"

#include <KDebug>
#include <KJob>
#include <Soprano/Vocabulary/NAO>


Nepomuk::PIMMergeService::PIMMergeService( QObject* parent, const QList< QVariant >& )
	: Service(parent)
{
    kDebug();
    queryEmailDuplicates(Nepomuk::Resource());
    queryPhoneDuplicates(Nepomuk::Resource());
    createPimoThings();

    Nepomuk::ResourceWatcher *ncoWatcher = new Nepomuk::ResourceWatcher(this);
    ncoWatcher->addType(Nepomuk::Vocabulary::NCO::Contact());

    connect(ncoWatcher, SIGNAL(resourceCreated(Nepomuk::Resource, QList<QUrl>)),
            this, SLOT(onNcoResourceCreated(Nepomuk::Resource, QList<QUrl>)));

    ncoWatcher->start();
}

Nepomuk::PIMMergeService::~PIMMergeService()
{

}

void Nepomuk::PIMMergeService::queryEmailDuplicates(const Nepomuk::Resource& resource)
{
    kDebug() << "Looking for email duplicates...";
    QString query;

    if (resource.isValid()) {
        query = QString("select distinct ?c1 ?a1 ?a2 "
        "(select count(*) where { ?c1 ?p ?o . }) as ?cnt1 "
        "(select count(*) where { %1 ?p ?o . }) as ?cnt2 where { "
        "?c1 a nco:Contact . "
        "?c1 nco:hasEmailAddress ?e1 . "
        "%1 nco:hasEmailAddress ?e2 . "
        "?e1 nco:emailAddress ?a1 . "
        "?e2 nco:emailAddress ?a2 . "
        "FILTER(?c1 < %1) . "
        "FILTER(?a1 = ?a2) . }").arg(Soprano::Node::resourceToN3(resource.uri()));
    } else {
        query = "select distinct ?c1 ?c2 ?a1 ?a2 "
        "(select count(*) where { ?c1 ?p ?o . }) as ?cnt1 "
        "(select count(*) where { ?c2 ?p ?o . }) as ?cnt2 where { "
        "?c1 a nco:Contact . "
        "?c2 a nco:Contact . "
        "?c1 nco:hasEmailAddress ?e1 . "
        "?c2 nco:hasEmailAddress ?e2 . "
        "?e1 nco:emailAddress ?a1 . "
        "?e2 nco:emailAddress ?a2 . "
        "FILTER(?c1 < ?c2) . "
        "FILTER(?a1 = ?a2) . }";
    }

    Soprano::QueryResultIterator queryResultIterator = Nepomuk::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                                       Soprano::Query::QueryLanguageSparql);

    mergeNCOs(queryResultIterator);
}

void Nepomuk::PIMMergeService::queryPhoneDuplicates(const Nepomuk::Resource& resource)
{
    kDebug() << "Looking for phone duplicates...";
    QString query;

    if (resource.isValid()) {
        query = QString("select distinct ?c1 ?a1 ?a2 "
        "(select count(*) where { ?c1 ?p ?o . }) as ?cnt1 "
        "(select count(*) where { %1 ?p ?o . }) as ?cnt2 where { "
        "?c1 a nco:Contact . "
        "?c1 nco:hasPhoneNumber ?e1 . "
        "%1 nco:hasPhoneNumber ?e2 . "
        "?e1 nco:phoneNumber ?a1 . "
        "?e2 nco:phoneNumber ?a2 . "
        "FILTER(?c1 < %1) . "
        "FILTER(?a1 = ?a2) . }").arg(Soprano::Node::resourceToN3(resource.uri()));
    } else {
        query = "select distinct ?c1 ?c2 ?a1 ?a2 "
        "(select count(*) where { ?c1 ?p ?o . }) as ?cnt1 "
        "(select count(*) where { ?c2 ?p ?o . }) as ?cnt2 where { "
        "?c1 a nco:Contact . "
        "?c2 a nco:Contact . "
        "?c1 nco:hasPhoneNumber ?e1 . "
        "?c2 nco:hasPhoneNumber ?e2 . "
        "?e1 nco:phoneNumber ?a1 . "
        "?e2 nco:phoneNumber ?a2 . "
        "FILTER(?c1 < ?c2) . "
        "FILTER(?a1 = ?a2) . }";
    }

    Soprano::QueryResultIterator queryResultIterator = Nepomuk::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                                       Soprano::Query::QueryLanguageSparql);

    mergeNCOs(queryResultIterator);
}

void Nepomuk::PIMMergeService::mergeNCOs(Soprano::QueryResultIterator& queryResultIterator)
{
    if (!queryResultIterator.isValid()) {
        return;
    }

    QMultiHash<int, int> cardinalityHash;
    QMultiHash<int, int> itemIdHash;

    while(queryResultIterator.next()) {
        int v1 = queryResultIterator["c1"].uri().toString().mid(14).toUInt();
        int v2 = queryResultIterator["c2"].uri().toString().mid(14).toUInt();

        int c1 = queryResultIterator["cnt1"].literal().toInt();
        int c2 = queryResultIterator["cnt2"].literal().toInt();

        if (!itemIdHash.contains(v1, v2)) {
            itemIdHash.insertMulti(v1, v2);
        }

        cardinalityHash.insert(v1, c1);
        cardinalityHash.insert(v2, c2);
    }

    QMultiHash<int, int> idHash;

    QHashIterator<int, int> idHashIterator(itemIdHash);
    idHashIterator.toBack();
    while (idHashIterator.hasPrevious()) {
        idHashIterator.previous();
        //         kDebug() << i.key() << i.value();

        if (!idHash.contains(idHashIterator.key(), idHashIterator.value())) {
            idHash.insertMulti(idHashIterator.key(), idHashIterator.value());
        }

        if (idHash.contains(idHashIterator.value())) {
            //             kDebug() << "Processing" << i.value(); // << intermediateHash.take(i.value());
            foreach (int n, idHash.values(idHashIterator.value())) {
                idHash.insertMulti(idHashIterator.value(), n);
                idHash.remove(idHashIterator.value(), n);
            }
        }
    }

    if (idHash.isEmpty()) {
        //nothing to merge
        return;
    }

    QHashIterator<int, int> idIterator(idHash);
    while (idIterator.hasNext()) {
        idIterator.next();
        kDebug() << idIterator.key() << ": " << idIterator.value() << endl;
        KJob *mergeJob = 0;

        //let the resource with more properties take precedence
        if (cardinalityHash.value(idIterator.key()) >= cardinalityHash.value(idIterator.value())) {
            mergeJob = Nepomuk::mergeResources(QUrl(QString("akonadi:?item=%1").arg(idIterator.key())),
                                               QUrl(QString("akonadi:?item=%1").arg(idIterator.value())));
        } else {
            mergeJob = Nepomuk::mergeResources(QUrl(QString("akonadi:?item=%1").arg(idIterator.value())),
                                               QUrl(QString("akonadi:?item=%1").arg(idIterator.key())));
        }

        if (mergeJob) {
            connect(mergeJob, SIGNAL(finished(KJob*)), this, SLOT(mergeFinished(KJob*)));
            mergeJob->start();
        }
    }
}


void Nepomuk::PIMMergeService::mergeFinished(KJob* job)
{
    if (job->error()) {
        kDebug() << job->errorString() << job->errorText();
    } else {
        kDebug() << "Merging done";
    }
}

void Nepomuk::PIMMergeService::onNcoResourceCreated(const Nepomuk::Resource &resource, const QList<QUrl> &stypes)
{
    kDebug();
    queryEmailDuplicates(resource);
    queryPhoneDuplicates(resource);
}

void Nepomuk::PIMMergeService::createPimoThings()
{
    kDebug();
    QString query;

    query = "select distinct ?c1 ?c2 ?Gname1 ?Fname1 ?Gname2 ?Fname2 "
    "where { "
    "?c1 a nco:Contact . "
    "?c2 a nco:Contact . "
    "?c1 nco:nameFamily ?Fname1 . "
    "?c2 nco:nameFamily ?Fname2 . "
    "OPTIONAL { ?c1 nco:nameGiven ?Gname1 } . "
    "OPTIONAL { ?c2 nco:nameGiven ?Gname2 } . "
    "FILTER( ( bound(?Gname1) && bound(?Gname2) && ?Gname1 LIKE ?Gname2) ). "
    "FILTER(?c1 < ?c2) . "
    "FILTER(?Fname1 = ?Fname2 || (?Fname1 = ?Gname2 && ?Fname2 = ?Gname1) ) . "
    "}";

    Soprano::QueryResultIterator it = Nepomuk::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                      Soprano::Query::QueryLanguageSparql);

    QMultiHash<QString, QString> uriHash;
    QMultiHash<int, int> iHash;
    while(it.next()) {
        int v1 = it["c1"].uri().toString().mid(14).toUInt();
        int v2 = it["c2"].uri().toString().mid(14).toUInt();

        if (!iHash.contains(v1, v2)) {
            iHash.insertMulti(v1, v2);
        }
    }

    QMultiHash<int, int> intermediateHash;

    QHashIterator<int, int> i(iHash);
    i.toBack();
    while (i.hasPrevious()) {
        i.previous();

        if (!intermediateHash.contains(i.key(), i.value())) {
            intermediateHash.insertMulti(i.key(), i.value());
        }

        if (intermediateHash.contains(i.value())) {
            foreach (int n, intermediateHash.values(i.value())) {
                intermediateHash.insertMulti(i.value(), n);
                intermediateHash.remove(i.value(), n);
            }
        }
    }

    Nepomuk::SimpleResourceGraph g;

    QHashIterator<int, int> i2(intermediateHash);
    foreach(int n, intermediateHash.uniqueKeys()) {

        Nepomuk::SimpleResource person;
        person.addType(Nepomuk::Vocabulary::PIMO::Person());

        //kDebug() << Nepomuk::Resource(QUrl(QString("akonadi:?item=%1").arg(n))).pimoThing().uri();

        Nepomuk::SimpleResource contact(QUrl(QString("akonadi:?item=%1").arg(n)));
        person.addProperty(Nepomuk::Vocabulary::PIMO::groundingOccurrence(), contact);

        QString prefLabel = Nepomuk::Resource(QUrl(QString("akonadi:?item=%1").arg(n))).property(Soprano::Vocabulary::NAO::prefLabel()).toString();

        kDebug() << n << ":" << intermediateHash.values(n) << endl;

        foreach(int v, intermediateHash.values(n)) {
            Nepomuk::SimpleResource contact2(QUrl(QString("akonadi:?item=%1").arg(v)));
            person.addProperty(Nepomuk::Vocabulary::PIMO::groundingOccurrence(), contact2);

            if (prefLabel.isEmpty()) {
                prefLabel = Nepomuk::Resource(QUrl(QString("akonadi:?item=%1").arg(v))).property(Soprano::Vocabulary::NAO::prefLabel()).toString();
            }
        }

        person.addProperty(Soprano::Vocabulary::NAO::prefLabel(), prefLabel);

        g << person;
    }

    KJob *storeJob = Nepomuk::storeResources(g);
    connect(storeJob, SIGNAL(finished(KJob*)), this, SLOT(mergeFinished(KJob*)));
    storeJob->start();
}

#include <kpluginfactory.h>
#include <kpluginloader.h>

NEPOMUK_EXPORT_SERVICE( Nepomuk::PIMMergeService, "nepomukpimmergeservice" )

#include "mergeservice.moc"
